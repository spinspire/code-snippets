<?php
header('Content-Type: text/plain');
$a1 = array(); // initialze an empty array
$a2 = array('foo', 'bar'); // initialize an array with array contructor
$a3 = ['foo', 'bar']; // array literal
$a4 = [2 => 'p2', 7 => 'p7', 'p8', 'p9', 1 => 'p1']; // initialize with specific indexes (as opposed to 0, 1, 2 ...
$a4[] = 'p10'; // append after the highest index
$a4[] = $a3;
$a4[] = 'p12';
unset($a4[8]); // remove the position/element/cell from array
// $a4[8] = NULL; // set the position/element/cell value to NULL

function print_array(array $a, $prefix = '') {
  print("------------BEGIN----------\n");
  foreach($a as $k => $v) {
    if(is_array($v)) {
      print_array($v, "[$k]"); // recursive function call
    } else {
      print("${prefix}[$k] = $v\n");
    }
  }
  print("------------ END ----------\n");
}

print_array($a4);

$a1[] = 'p0';
$a1[] = 'p1';
$a1[] = 'p2';
$a1[] = 'p3';
unset($a1[2]); // unset leaves a "hole" in the array
print_array($a1);
$p0 = array_shift($a1); // removes and returns value from front of the array (and re-index the array)
print_array($a1);
array_unshift($a1, 'new p0'); // inserts a new value to the front of the array (and moves all others up on position)
print_array($a1);

if(1 == '1') {
  print("1 == '1' is TRUE\n");
} else {
  print("1 == '1' is FALSE\n");
}

if(1 === '1') {
  print("1 === '1' is TRUE\n");
} else {
  print("1 === '1' is FALSE\n");
}

$a5 = [TRUE, 1, '1', FALSE, 0, '0', '', 'foo', 'FALSE'];
for($i = 0; $i < count($a5); $i++) {
  // comparison with
  if($a5[$i] == TRUE) {
    print("[$i] = $a5[$i] is TRUE\n");
  } else {
    print("[$i] = $a5[$i] is FALSE\n");
  }
}

for($i = 0; $i < count($a5); $i++) {
  if(TRUE === $a5[$i]) {
    print("[$i] = $a5[$i] is TRUE\n");
  } else {
    print("[$i] = $a5[$i] is FALSE\n");
  }
}
