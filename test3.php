<?php
header('Content-Type: text/plain');

// session, GET and POST requests

/* shows that $_SESSION is remembered from request to request */
session_start();
print("********* SESSION ************\n");
$_SESSION['request_times'][] = $_SERVER['REQUEST_TIME'];
print_r($_SESSION);

/* shows that $_REQUEST is forgotten from request to request */
print("********* REQUEST ************\n");
$_REQUEST['request_times'][] = $_SERVER['REQUEST_TIME'];
print_r($_REQUEST);

print("********* GET ************\n");
print_r($_GET);


// test with ...
// curl -X POST --data 'bob=alice&jeff=jane' 'http://localhost/test3.php?key=value&x=x1&foo=bar'
print("********* POST ************\n");
print_r($_POST);
