<?php
header('Content-Type: text/plain');
function f1_by_val($p1) {
  $p1 = 2*$p1;
}

function f2_by_ref(&$p1) {
  $p1 = 2*$p1;
}

$v1 = 3;
print("\$v1 = $v1\n");
f1_by_val($v1);
print("\$v1 = $v1\n");

$v2 = 5;
print("\$v2 = $v2\n");
f2_by_ref($v2);
print("\$v2 = $v2\n");

print(4/3 . "\n");
print(4 % 3 . "\n");
print("1" + "3");
print("\n");
print("1" . 3);