<?php
// super-globals
// print($_SERVER); #this doesn't work because $_SERVER is an array. You need a string.
header('Content-Type: text/plain');
print("**************************************\nPRINT_R\n**************************************\n");
print_r($_SERVER); #this works. Because print_r prints the array into a string first

function print_array_key_values($a, $name) {
  print("**************************************\nFOREACH LOOP: $name\n**************************************\n");
  foreach($a as $bar => $foo) {
    print($bar); // key
    print('=');
    print($foo); // value
    print("\n");
  }
}

// start the session before using $_SESSION
session_start();

print_array_key_values($_SERVER, 'SERVER');
print_array_key_values($_COOKIE, 'COOKIE');
print_array_key_values($_ENV, 'ENV');
print_array_key_values($_FILES, 'FILES');
print_array_key_values($_GET, 'GET');
print_array_key_values($_POST, 'POST');
print_array_key_values($_REQUEST, 'REQUEST');
print_array_key_values($_SESSION, 'SESSION');
