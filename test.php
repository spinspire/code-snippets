<?php
// phpinfo();
/*
Multi-line comment.
Another line.
*/
/**
 * Doc comment.
 * Can be multi-line.
 */
// literals .. 1, 125, 'foo bar', "foo bar", TRUE, FALSE, [1, 'foo', 125], ['x' => 'x1', 'y' => 'y1']
$name = 'World'; // string variable assignment
// tell the client (browser) that the output format (mime-type) is plain text
header('Content-Type: text/plain');
print("Hello $name\n\n\n\n"); # double-quotes allow string interpolation, as well escape sequences
echo('Hello $name\n'); // no string interpolation (substitution) for single-quotes

// $this->t('Hello @name', ['@name' => $name]);